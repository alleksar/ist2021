const express = require("express");
const fs=require("fs");
const app = express();
const path = require('path');
const axios = require('axios');
const port = 2000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

let ucitajPogled=(imePogleda)=>{
    return fs.readFileSync(path.join(__dirname+"/pogledi/"+imePogleda+".html"),"utf-8")
}

app.get("/",(req,res)=>{
    axios.get('http://localhost:1000/sviProizvodi')
    .then(response => {
        let podaci="";
        response.data.forEach(element => {
            podaci+=
            `<tr>
                <td>${element.id}</td>
                <td>${element.tip}</td>
                <td>${element.naziv}</td>
                <td>${element.cena}</td>
            </tr>`;
        });
        
        res.send(ucitajPogled("index").replace("#{data}",podaci));
    })
    .catch(error => {
        console.log(error);
    });
});

app.get("/dodajproizvodpogled",(req,res)=>{
    res.send(ucitajPogled("dodajproizvod"));
});

app.post("/dodajproizvod",(req,res)=>{
    axios.post("http://localhost:1000/dodajproizvod",{
        tip:req.body.tip,
        naziv:req.body.naziv,
        cena:req.body.cena
    })
    res.redirect("/");
});

app.post("/filtrirajproizvode",(req,res)=>{
    axios.get(`http://localhost:1000/dohvatiproizvodetip/${req.body.tip}`)
    .then(response=>{
        let podaci="";
        response.data.forEach(element => {
            podaci+=
            `<tr>
                <td>${element.id}</td>
                <td>${element.tip}</td>
                <td>${element.naziv}</td>
                <td>${element.cena}</td>
            </tr>`;
        });
        
        res.send(ucitajPogled("index").replace("#{data}",podaci));
    })
});

app.get("/obrisiproizvodpogled",(req,res)=>{
    axios.get('http://localhost:1000/sviProizvodi')
    .then(response => {
        let podaci="";
        response.data.forEach(element => {
            podaci+=
            `<tr>
                <td><a href="/obrisiproizvod/${element.id}">${element.id}</a></td>
                <td>${element.tip}</td>
                <td>${element.naziv}</td>
                <td>${element.cena}</td>
            </tr>`;
        });
        
        res.send(ucitajPogled("obrisiproizvod").replace("#{data}",podaci));
    })
    .catch(error => {
        console.log(error);
    });
});

app.get("/obrisiproizvod/:id",(req,res)=>{
    axios.delete(`http://localhost:1000/obrisiproizvod/${req.params["id"]}`);
    res.redirect("/obrisiproizvodpogled");
});

app.listen(port,()=>{console.log(`Klijent podignut port: ${port}`)});