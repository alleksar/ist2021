var express = require('express');
var proizvodiServis=require('webprodavnicamodul');
var app = express();
const port = 1000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get('/',(request, response)=>{
    response.send(proizvodiServis.sviProizvodi());
});

app.get('/sviProizvodi',(request, response)=>{
    response.send(proizvodiServis.sviProizvodi())
});

app.post('/dodajproizvod',(request, response)=>{
    proizvodiServis.dodajProizvod(request.body);
    response.end("Proizvod Dodat");
})

app.put('/postavinaziv/:id/:naziv',(request, response)=>{
    proizvodiServis.postaviNazivProizvoda(request.params["id"],request.params["naziv"]);
    response.end("Naziv Proizvoda Azuriran");
});

app.delete('/obrisiproizvod/:id',(request, response)=>{
    proizvodiServis.obrisiProizvod(request.params["id"]);
    response.end("Proizvod Obrisan");
});

app.get('/dohvatiproizvodetip/:tip',(request, response)=>{
    response.send(proizvodiServis.vratiProizvodeTip(request.params["tip"]));
});

app.get('/vratiproizvodid/:id',(request, response)=>{
    response.send(proizvodiServis.vratiProizvodID(request.params["id"]));
})


app.listen(port,()=>{console.log(`Server podignut! port: ${port}`)});