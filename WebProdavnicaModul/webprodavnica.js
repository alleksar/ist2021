const fs = require('fs');
const path = require('path');
const dirPath = path.join(__dirname, '/proizvodi.json');

exports.sviProizvodi = () => {
    return ProcitajProizvode();
}

exports.dodajProizvod = (noviProizvod) => {
    let id=1;
    let proizvodi=this.sviProizvodi();
    if(proizvodi.length>0){
        id=proizvodi[proizvodi.length-1].id+1;
    }
    noviProizvod.id=id;
    proizvodi.push(noviProizvod);
    snimiProizvode(proizvodi);
}

exports.vratiProizvodID = (id) => {
    return this.sviProizvodi().find(proizvod => proizvod.id == id);
}

exports.postaviNazivProizvoda = (id,naziv) => {
    let proizvodi=this.sveKnjige().filter(proizvod=>proizvod.id==id);
    proizvodi.forEach(element => {
        element.naziv=naziv;
    });
    snimiProizvode(proizvodi);
}

exports.obrisiProizvod = (id) => {
    let proizvodi = this.sviProizvodi().filter(proizvod=>proizvod.id!=id);
    snimiProizvode(proizvodi);
}

exports.vratiProizvodeTip = (tip) =>{
    return this.sviProizvodi().filter(proizvod=>proizvod.tip==tip);
}

let ProcitajProizvode=()=>{
    let proizvodi=fs.readFileSync(dirPath, (err, data) => {
        if (err) throw err;
        return data;
    });
    return JSON.parse(proizvodi);
}

let snimiProizvode=(proizvodi)=>{
    fs.writeFileSync(dirPath,JSON.stringify(proizvodi));
}